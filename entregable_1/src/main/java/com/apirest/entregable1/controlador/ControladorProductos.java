package com.apirest.entregable1.controlador;

import com.apirest.entregable1.modelo.ModeloProducto;
import com.apirest.entregable1.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${url.base}")
public class ControladorProductos {


    @Autowired
    private ServicioDatos servicioDatos;

    // CONSULTA PRODUCTOS

    @GetMapping("/")
    public ResponseEntity obtenerProductos()
    {
        return ResponseEntity.ok(this.servicioDatos.obtenerProductos());
    }



    // CONSULTA PRODUCTOS POR ID

    @GetMapping("/{idProducto}")
    public ResponseEntity obtenerProductos(@PathVariable int idProducto)
    {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorID(idProducto);
        if (p != null)
        {
            return ResponseEntity.ok(p);
        }

        return new ResponseEntity<>("Producto: "+idProducto+ " no encontrado", HttpStatus.NOT_FOUND);
    }


    // CREA PRODUCTO
    @PostMapping("/")
    public ResponseEntity createProducto(@RequestBody ModeloProducto producto)
    {
        final ModeloProducto p = this.servicioDatos.agregarproducto(producto);
        return new ResponseEntity<>("Producto " + p.getId() +" Creado Satifactoriamente!", HttpStatus.CREATED);
    }

    // ACTUALIZA PRODUCTO
    @PutMapping("/{idProducto}")
    public ResponseEntity actualizaProducto(@PathVariable int idProducto, @RequestBody ModeloProducto producto)
    {
        final  ModeloProducto p = this.servicioDatos.actualizaProducto(idProducto,producto);
        if(p != null)
        {
            return new ResponseEntity<>("Producto: "+ idProducto +" actualizado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Producto: " + idProducto +" no encontrado.", HttpStatus.NOT_FOUND);

    }


    // BORRA PRODUCTO
    @DeleteMapping("/{idProducto}")
    public ResponseEntity borrarProducto(@PathVariable int idProducto)
    {
        final ModeloProducto p = this.servicioDatos.borrarProducto(idProducto);
        if(p!=null)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Producto: " + idProducto + " no encontrado.", HttpStatus.NOT_FOUND);
    }

    // ACTUALIZACION PARCIAL DE PRODUCTO
    @PatchMapping("/{idProducto}")
    public ResponseEntity actualizaParteProducto(@PathVariable int idProducto, @RequestBody ModeloProducto producto)
    {
        ModeloProducto p = this.servicioDatos.actualizaParteProducto(idProducto,producto);
        if(p != null )
        {
            return new ResponseEntity<>(p, HttpStatus.OK);
        }
        return new ResponseEntity<>("Producto: " +  idProducto + " no encontrado.", HttpStatus.NOT_FOUND);
    }




}
