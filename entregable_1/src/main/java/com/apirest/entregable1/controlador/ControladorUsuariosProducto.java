package com.apirest.entregable1.controlador;

import com.apirest.entregable1.modelo.ModeloProducto;
import com.apirest.entregable1.modelo.ModeloUsuario;
import com.apirest.entregable1.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ControladorUsuariosProducto {


    @Autowired
    private ServicioDatos servicioDatos;

    // CONSULTA USUARIOS DE PRODUCTO
    @GetMapping("/{idProducto}/usuarios")
    public ResponseEntity obtenerUsuariosProducto(@PathVariable int idProducto) {
        try {

            final ModeloProducto p = this.servicioDatos.obtenerProductoPorID(idProducto);
            final List<ModeloUsuario> l;

            if (p==null)
            {
                return new ResponseEntity<>("Producto: " + idProducto+ " no encontrado.", HttpStatus.NOT_FOUND);
            }

            l = this.servicioDatos.obtenerProductoUsuarios(idProducto);

            if (l.size()>0)
            {
                return ResponseEntity.ok(l);
            }

            //  return new ResponseEntity<>("Producto: "+ idProducto+ " no tiene usuarios ",HttpStatus.NO_CONTENT);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (Exception x) {
            //return new ResponseEntity(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>("Producto: "+ idProducto+ " no tiene usuarios ",HttpStatus.NOT_FOUND);
        }
    }

    // CONSULTA UN USUARIOS DE PRODUCTO
    @GetMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity obtenerUsuariosProducto(@PathVariable int idProducto, @PathVariable int idUsuario) {

        final ModeloProducto p = this.servicioDatos.obtenerProductoPorID(idProducto);
        final ModeloUsuario u;

        if (p==null)
        {
            return new ResponseEntity<>("Producto: " + idProducto+ " no encontrado.", HttpStatus.NOT_FOUND);
        }

        u = this.servicioDatos.obtenerProductoUsuario(idProducto, idUsuario);

        return (u == null)
                ? new ResponseEntity<>("Usuario: "+idUsuario+ " no encontrado",HttpStatus.NOT_FOUND)
                : ResponseEntity.ok(u);
    }

    // CREA USUARIO
    @PostMapping("/{idProducto}/usuarios")
    public ResponseEntity agregarUsuarioProducto(@PathVariable int idProducto, @RequestBody ModeloUsuario usuario) {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorID(idProducto);

        if (p == null) {
            return new ResponseEntity<>("Producto:  " + idProducto + " no existe",HttpStatus.NOT_FOUND);
        }
        this.servicioDatos.agregarUsuarioProducto(idProducto, usuario);
        return new ResponseEntity<>("Usuario: " + usuario.getId()+ " creado satisfactoriamente!", HttpStatus.CREATED);
    }

    // ACTUALIZA USUARIO
    @PutMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity actualizaUsuarioDeProducto(@PathVariable int idProducto,@PathVariable int idUsuario, @RequestBody ModeloUsuario usuario)
    {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorID(idProducto);
        final ModeloUsuario u;

        if (p==null)
        {
            return new ResponseEntity<>("Producto: " + idProducto+ " no encontrado.", HttpStatus.NOT_FOUND);
        }


        u =  this.servicioDatos.actualizaUsuarioDeProducto(idProducto,idUsuario,usuario);
        if(u!=null)
        {
            return new ResponseEntity<>("Usuario: "+ idUsuario +  " actualizado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Usuario: "+ idUsuario + " no encontrado.", HttpStatus.NOT_FOUND);
    }


    // BORRA USUARIO
    @DeleteMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity borraUsuarioProducto(@PathVariable int idProducto,@PathVariable int idUsuario)
    {
        final ModeloUsuario u = this.servicioDatos.borrarUsuarioDeProductoObj(idProducto,idUsuario);
        if(u != null)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Usuario : "+ idUsuario +" no encontrado.", HttpStatus.NOT_FOUND);
    }


    // ACTUALIZA PARTE USUARIO
    @PatchMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity actualizaParteUsuarioDeProducto(@PathVariable int idProducto,@PathVariable int idUsuario, @RequestBody ModeloUsuario usuario)
    {
        final ModeloUsuario u =  this.servicioDatos.actualizaParteUsuarioDeProducto(idProducto,idUsuario,usuario);
        if(u!=null)
        {
            return new ResponseEntity<>(u, HttpStatus.OK);
        }
        return new ResponseEntity<>("Usuario: "+ idUsuario + " no encontrado.", HttpStatus.NOT_FOUND);
    }

}
