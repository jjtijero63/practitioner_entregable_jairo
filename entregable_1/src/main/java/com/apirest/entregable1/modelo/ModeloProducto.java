package com.apirest.entregable1.modelo;

import java.util.ArrayList;
import java.util.List;

public class ModeloProducto {

    private int id;
    private String marca;
    private String descripcion;
    private double precio;
    private final List<ModeloUsuario> usuarios;

    public ModeloProducto(int id, String marca, String descripcion, double precio) {
        this.id = id;
        this.marca = marca;
        this.descripcion = descripcion;
        this.precio = precio;
        this.usuarios =  new ArrayList<>();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

/*
    public void setUsuarios(List<ModeloUsuario> usuarios) {
        this.usuarios = usuarios;
    }*/

    public int getId() {
        return id;
    }

    public String getMarca() {
        return marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public List<ModeloUsuario> getUsuarios() {
        return usuarios;
    }

}
