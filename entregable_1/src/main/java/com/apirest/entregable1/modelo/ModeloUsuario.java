package com.apirest.entregable1.modelo;

public class ModeloUsuario {

    int id;
    public  String nombre;
    public  String apellido;
    public  int edad;

    public ModeloUsuario(int id,String nombre,String apellido,int edad)
    {
        this.id=id;
        this.nombre=nombre;
        this.apellido=apellido;
        this.edad=edad;
    }

    public String getNombre() {
        return this.nombre;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() { return this.apellido; }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() { return this.edad; }

    public void setEdad(int edad) { this.edad = edad; }
}
