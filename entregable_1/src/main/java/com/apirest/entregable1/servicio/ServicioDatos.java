package com.apirest.entregable1.servicio;

import com.apirest.entregable1.modelo.ModeloProducto;
import com.apirest.entregable1.modelo.ModeloUsuario;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.util.ArrayUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component

public class ServicioDatos {

    private final AtomicInteger secuenciaIDProductos = new AtomicInteger(0);
    private final AtomicInteger secuenciaIDUsuarios = new AtomicInteger(0);
    private final List<ModeloProducto> productos = new ArrayList<ModeloProducto>();


    //********************************LEER***********************************/

    //Leer coleccion de productos
    public List<ModeloProducto> obtenerProductos() {
        return Collections.unmodifiableList(this.productos);
        //return this.productos;
    }

    //Leer coleccion de usuarios por producto
    public List<ModeloUsuario> obtenerProductoUsuarios(int idProducto) {

        return Collections.unmodifiableList(this.obtenerProductoPorID(idProducto).getUsuarios());
    }


    //Leer in producto por ID
    public ModeloProducto obtenerProductoPorID(int id) {
        for (ModeloProducto p : this.productos) {
            if (p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    public ModeloUsuario obtenerProductoUsuario(int idProducto, int idUsuario) {
        for (ModeloUsuario u : this.obtenerProductoPorID(idProducto).getUsuarios()) {
            if (u.getId() == idUsuario) {
                return u;
            }
        }
        return null;
    }
//********************************LEER***********************************//

//********************************CREA***********************************//


    public ModeloProducto agregarproducto(ModeloProducto producto) {
        producto.setId(this.secuenciaIDProductos.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }

    public ModeloUsuario agregarUsuarioProducto(int idProducto, ModeloUsuario usuario) {
        usuario.setId(this.secuenciaIDProductos.incrementAndGet());
        this.obtenerProductoPorID(idProducto).getUsuarios().add(usuario);
        return usuario;

    }


//********************************CREA***********************************//

//********************************ACTUALIZAR***********************************//


    public ModeloProducto actualizaProducto(int id, ModeloProducto producto) {
        for (int i = 0; i < this.productos.size(); i++) {
            if (this.productos.get(i).getId() == id) {

                producto.setId(id);
                // this.productos.get(i).setMarca(producto.getMarca());
                //this.productos.get(i).setDescripcion(producto.getDescripcion());
                //this.productos.get(i).setPrecio(producto.getPrecio());
                this.productos.set(i, producto);
                return producto;
            }
        }
        return null;
    }

    public ModeloProducto actualizaParteProducto(int id, ModeloProducto producto) {
        for (int i = 0; i < this.productos.size(); i++) {
            if (this.productos.get(i).getId() == id) {

                if (producto.getMarca() != null && producto.getMarca().trim().length() > 0) {
                    this.productos.get(i).setMarca(producto.getMarca());
                }
                if (producto.getDescripcion() != null && producto.getDescripcion().trim().length() > 0) {
                    this.productos.get(i).setDescripcion(producto.getDescripcion());
                }
                if (producto.getPrecio() == 0) {
                    this.productos.get(i).setPrecio(producto.getPrecio());
                }


                //this.productos.set(i, producto);
                return this.productos.get(i);
            }
        }
        return null;
    }


    public ModeloUsuario actualizaUsuarioDeProducto(int idProducto, int idUsuario, ModeloUsuario usuario) {

        List<ModeloUsuario> usuarios = this.obtenerProductoPorID(idProducto).getUsuarios();
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getId() == idUsuario) {
                usuario.setId(idUsuario);
                usuarios.set(i,usuario);
                return usuario;
            }
        }
        return null;
    }


    public ModeloUsuario actualizaParteUsuarioDeProducto(int idProducto, int idUsuario, ModeloUsuario usuario) {

        try {
            List<ModeloUsuario> usuarios = this.obtenerProductoPorID(idProducto).getUsuarios();
            for (int i = 0; i < usuarios.size(); i++) {
                if (usuarios.get(i).getId() == idUsuario) {

                    if (usuario.getNombre() != null && usuario.getNombre().trim().length() > 0) {
                        usuarios.get(i).setNombre(usuario.getNombre());
                    }
                    if (usuario.getApellido() != null && usuario.getApellido().trim().length() > 0) {
                        usuarios.get(i).setApellido(usuario.getApellido());
                    }
                    if (usuario.getEdad() > 0) {
                        usuarios.get(i).setEdad(usuario.getEdad());
                    }
                    return usuarios.get(i);
                }

            }
            return null;
        }catch (Exception e) {return null;}

    }

//********************************ACTUALIZAR***********************************//

//********************************BORRAR***********************************//

//DELETE
    //BORRAR UN PRODUCTO

    public ModeloProducto borrarProducto(int idProducto) {
        ModeloProducto p = this.obtenerProductoPorID(idProducto);

        if (p != null)
        {
            this.productos.remove(p);
            return p;
        }

        /*
        for (int i = 0; i < this.productos.size(); i++) {
            if (this.productos.get(i).getId() == id) {
                this.productos.remove(i);
                return true;
            }
        }

         */
        return null;
    }

    //BORRAR UN USUARIO DE UN PRODUCTO
    public boolean borrarUsuarioDeProducto(int idProducto, int idUsuario) {
        List<ModeloUsuario> usuarios = this.obtenerProductoPorID(idProducto).getUsuarios();
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getId() == idUsuario) {
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }

    public ModeloUsuario borrarUsuarioDeProductoObj(int idProducto, int idUsuario) {
        try {
            List<ModeloUsuario> usuarios = this.obtenerProductoPorID(idProducto).getUsuarios();
            ModeloUsuario u = this.obtenerProductoUsuario(idProducto, idUsuario);

            if (usuarios.remove(u)) {
                return u;
            }

            return null;
        } catch (Exception e) {return null;}
    }
//********************************BORRAR***********************************//

}
