package com.apirest.entregable2.controlador;


import com.apirest.entregable2.modelo.Producto;
import com.apirest.entregable2.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public List<Producto> obtenerProductos()
    {


        return this.servicioProducto.obtenerProductos();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Producto agregarProducto(@RequestBody Producto producto)
    {
        return this.servicioProducto.crearProducto(producto);
    }



    @GetMapping("/{id}")
    public ResponseEntity obtenerProducto(@PathVariable String id)
    {
        Optional<Producto> p = this.servicioProducto.obtenerProductoPorId(id);
        if(p.isPresent())
        {
            return ResponseEntity.ok(p.get());
        }
        return new ResponseEntity<>("Producto: "+  id +  " no encontrado",HttpStatus.NOT_FOUND);
    }


    @PutMapping
    public ResponseEntity  actualizaProducto(@RequestBody Producto producto)
    {

        Producto p = this.servicioProducto.actualizarProducto(producto);

        if (p != null)
        {
            return new ResponseEntity<>("Producto: "+ producto.getId() +  " actualizado correctamente.", HttpStatus.OK);

        }


        return new ResponseEntity<>("Producto: "+ producto.getId() + " no encontrado.", HttpStatus.NOT_FOUND);

    }


    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity actualizaParteProducto(@RequestBody Producto producto)
    {
        Optional <Producto> cp = this.servicioProducto.obtenerProductoPorId(producto.getId());


        if (cp.isPresent())
        {
            Producto p = cp.get();
            this.servicioProducto.actualizarParteProducto(producto,p);
            return new ResponseEntity<>("Producto: "+ producto.getId() +  " actualizado correctamente.", HttpStatus.OK);

        }


        return new ResponseEntity<>("Producto: "+ producto.getId() + " no encontrado.", HttpStatus.NOT_FOUND);


    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity borrarProducto(@PathVariable String id)
    {
        Optional <Producto> cp = this.servicioProducto.obtenerProductoPorId(id);

        if (cp.isPresent())
        {
            this.servicioProducto.borrarProductoPorID(id);
            return new ResponseEntity<>("Producto: "+ id +  " borrado correctamente.", HttpStatus.OK);

        }
        return new ResponseEntity<>("Producto: "+ id + " no encontrado.", HttpStatus.NOT_FOUND);


    }



}
