package com.apirest.entregable2.controlador;


import com.apirest.entregable2.modelo.Producto;
import com.apirest.entregable2.modelo.Usuario;
import com.apirest.entregable2.servicio.ServicioProducto;
import com.apirest.entregable2.servicio.ServicioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2/productos/{idProducto}/usuarios")
public class ControladorUsuariosProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    ServicioUsuario servicioUsuario;



    @GetMapping
    public ResponseEntity obtenerUsuariosDeProductos(@PathVariable String idProducto){
        try
        {

            return ResponseEntity.ok(this.servicioProducto.obtenerUsuariosProducto(idProducto));



        } catch (IllegalArgumentException x) {
            return new ResponseEntity<>("Producto: "+idProducto+ " no encontrado", HttpStatus.NOT_FOUND);

        }
    }


    @PostMapping
    public ResponseEntity creaUsuarioDeProducto(@PathVariable String idProducto, @RequestBody Usuario usuario)
    {

        Optional<Producto> p = this.servicioProducto.obtenerProductoPorId(idProducto);
        final Usuario u;

        if (!p.isPresent())
        {
            return new ResponseEntity<>("Producto: " + idProducto+ " no encontrado.", HttpStatus.NOT_FOUND);
        }


        u =  this.servicioUsuario.creaUsuarioDeProducto(p.get(),usuario);
        if(u!=null)
        {
            return new ResponseEntity<>("Usuario: "+ usuario.getId() +  " creado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Usuario: "+ usuario.getId() + " no creado.", HttpStatus.NOT_FOUND);
    }





    //("/{idUsuario}")
    @PutMapping
    public ResponseEntity actualizaUsuarioDeProducto(@PathVariable String idProducto, @RequestBody Usuario usuario)
    {
        System.out.println(" --- actualizaUsuarioDeProducto xx:  " + idProducto);
        Optional<Producto> p = this.servicioProducto.obtenerProductoPorId(idProducto);
        final Usuario u;

        if (!p.isPresent())
        {
            return new ResponseEntity<>("Producto: " + idProducto+ " no encontrado.", HttpStatus.NOT_FOUND);
        }


        u =  this.servicioUsuario.actualizaUsuarioDeProducto(p.get(),usuario);
        if(u!=null)
        {
            return new ResponseEntity<>("Usuario: "+ usuario.getId() +  " actualizado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Usuario: "+ usuario.getId() + " no encontrado.", HttpStatus.NOT_FOUND);
    }


    @PatchMapping("/{idUsuario}")
    public ResponseEntity actualizaParteUsuarioDeProducto(@PathVariable String idProducto, @PathVariable int idUsuario, @RequestBody Usuario usuario)
    {
        System.out.println(" --- PatchMapping xx:  " + idProducto);
        Optional<Producto> p = this.servicioProducto.obtenerProductoPorId(idProducto);
        final Usuario u;
        usuario.setId(idUsuario);

        if (!p.isPresent())
        {
            return new ResponseEntity<>("Producto: " + idProducto+ " no encontrado.", HttpStatus.NOT_FOUND);
        }


        u =  this.servicioUsuario.actualizaParteUsuarioDeProducto(p.get(),/*idUsuario,*/usuario);
        if(u!=null)
        {
            return new ResponseEntity<>("Usuario: "+ idUsuario +  " actualizado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Usuario: "+ idUsuario + " no encontrado.", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{idUsuario}")
    public ResponseEntity borrarUsuarioDeProducto(@PathVariable String idProducto, @PathVariable long idUsuario/*, @RequestBody Usuario usuario*/)
    {
        System.out.println(" --- PatchMapping xx:  " + idProducto);
        Optional<Producto> p = this.servicioProducto.obtenerProductoPorId(idProducto);
        final boolean u;

        if (!p.isPresent())
        {
            return new ResponseEntity<>("Producto: " + idProducto+ " no encontrado.", HttpStatus.NOT_FOUND);
        }


        u =  this.servicioUsuario.borrarUsuarioDeProducto(p.get(),idUsuario/*,usuario*/);
        if(u ==true)
        {
            return new ResponseEntity<>("Usuario: "+ idUsuario +  " borrado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Usuario: "+ idUsuario + " no encontrado.", HttpStatus.NOT_FOUND);


    }


/*
    @GetMapping
    public List<Usuario> obtenerUsuarioDeProductos(@PathVariable String idProducto,@PathVariable String idUsuario){
        try
        {
            return this.servicioProducto.obtenerUsuariosProducto(idProducto);

        } catch (IllegalArgumentException x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getLocalizedMessage());
        }
    }
 */
}
