package com.apirest.entregable2.datos;

import com.apirest.entregable2.modelo.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioUsuario extends MongoRepository<Usuario,String> {
}
