package com.apirest.entregable2.datos;

import com.apirest.entregable2.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioProducto  extends MongoRepository<Producto,String> {
}
