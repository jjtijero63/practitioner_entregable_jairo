package com.apirest.entregable2.servicio.impl;

import com.apirest.entregable2.datos.RepositorioProducto;
import com.apirest.entregable2.datos.RepositorioUsuario;
import com.apirest.entregable2.modelo.Producto;
import com.apirest.entregable2.modelo.Usuario;
import com.apirest.entregable2.servicio.ServicioProducto;
import com.apirest.entregable2.servicio.ServicioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ServicioUsuarioImpl implements ServicioUsuario {


    @Autowired
    private RepositorioUsuario repositorioUsuario;

    @Autowired
    private RepositorioProducto repositorioProducto;

    @Autowired
    ServicioProducto servicioProducto;

    //private final AtomicLong secuencuiaIds= new AtomicLong(0L);
    private final AtomicInteger secuencuiaIds = new AtomicInteger(0);


    @Override
    public Usuario creaUsuarioDeProducto (Producto p , Usuario usuario)
    {
            System.out.println("this.secuencuiaIds.incrementAndGet() ::" + this.secuencuiaIds.incrementAndGet());
            usuario.setId(this.secuencuiaIds.incrementAndGet());
            p.getUsuarios().add(usuario);
            this.repositorioProducto.save(p);
            return  usuario;
    }



    @Override
    public Usuario actualizaUsuarioDeProducto (Producto p , Usuario usuario)
    {
        final List<Usuario> usuarios = p.getUsuarios();
        if  (usuarios != null)
        {

            for (int i=0; i< p.getUsuarios().size();i++)
            {
                if(p.getUsuarios().get(i).getId() == usuario.getId())
                {
                    p.getUsuarios().set(i,usuario);
                    this.repositorioProducto.save(p);
                    return  usuario;
                }
            }



        }
        return null;
    }


    @Override
    public Usuario actualizaParteUsuarioDeProducto (Producto p ,/*String idUsuario,*/ Usuario usuario)
    {
        final List<Usuario> usuarios = p.getUsuarios();
        if  (usuarios != null)
        {

            for (int i=0; i< p.getUsuarios().size();i++)
            {
                if(p.getUsuarios().get(i).getId() == usuario.getId())
                {
                    //p.getUsuarios().set(i,usuario);

                   // if (usuario.getId() != null && usuario.getId().trim().length() > 0) {
                   //     p.getUsuarios().get(i).setId(usuario.getId());
                   // }
                    if (usuario.getNombre() != null && usuario.getNombre().trim().length() > 0) {
                        p.getUsuarios().get(i).setNombre(usuario.getNombre());
                    }
                    if (usuario.getRol() != null && usuario.getRol().trim().length() > 0) {
                        p.getUsuarios().get(i).setRol(usuario.getRol());
                    }
                    this.repositorioProducto.save(p);
                    return  usuario;
                }
            }



        }
        return null;
    }


    @Override
    public boolean borrarUsuarioDeProducto (Producto p ,long idUsuario/*, Usuario usuario*/)
    {
        final List<Usuario> usuarios = p.getUsuarios();
        if  (usuarios != null)
        {

            for (int i=0; i< p.getUsuarios().size();i++)
            {
                if(p.getUsuarios().get(i).getId() == idUsuario)
                {
                    System.out.println("borrar dddd: " + i);
                    p.getUsuarios().remove(i);
                    System.out.println("borrar dddd");
                    this.repositorioProducto.save(p);
                    return  true;
                }
            }



        }
        return false;
    }


}
