package com.apirest.entregable2.servicio.impl;


import com.apirest.entregable2.datos.RepositorioProducto;
import com.apirest.entregable2.modelo.Producto;
import com.apirest.entregable2.modelo.Usuario;
import com.apirest.entregable2.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    @Autowired
    private RepositorioProducto repositorioProducto;



    @Override
    public Producto crearProducto(Producto producto)
    {

        producto.setUsuarios(new ArrayList<Usuario>());
        return  this.repositorioProducto.insert(producto);
    }

    @Override
    public List<Producto> obtenerProductos()
    {
        return  this.repositorioProducto.findAll();
    }


    @Override
    public Optional<Producto> obtenerProductoPorId(String id)
    {
        return  this.repositorioProducto.findById(id);
    }


    @Override
    public Producto actualizarProducto(Producto producto)
    {
        Optional<Producto> cp = this.obtenerProductoPorId(producto.getId());
        if(!cp.isPresent())
        {
            return null;
        }
        Producto p = cp.get();
        producto.setUsuarios(p.getUsuarios());

        return  this.repositorioProducto.save(producto);
    }

    @Override
    public Producto actualizarParteProducto(Producto producto,Producto productoActualizar)
    {

        if (producto.getMarca() != null && producto.getMarca().trim().length() > 0) {
            productoActualizar.setMarca(producto.getMarca());
        }
        if (producto.getDescripcion() != null && producto.getDescripcion().trim().length() > 0) {
            productoActualizar.setDescripcion(producto.getDescripcion());
        }
        if (producto.getPrecio() == 0) {
            productoActualizar.setPrecio(producto.getPrecio());
        }


        //this.productos.set(i, producto);
        return this.repositorioProducto.save(productoActualizar);
    }

    @Override
    public  void borrarProductoPorID(String id)
    {
        this.repositorioProducto.deleteById(id);
    }



    @Override
    public List<Usuario> obtenerUsuariosProducto(String idProducto)
    {
        System.out.println("entro--obtenerUsuarioProducto");
        Optional<Producto> cp = this.obtenerProductoPorId(idProducto);
        if(!cp.isPresent())
            throw  new IllegalArgumentException("No existe el producto");
        Producto p = cp.get();
        final List<Usuario> usuarios = p.getUsuarios();

        return usuarios==null
                ? Collections.emptyList()
                : p.getUsuarios()
                ;

    }


}
