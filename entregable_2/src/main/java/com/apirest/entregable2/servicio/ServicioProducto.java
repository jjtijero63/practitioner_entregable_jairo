package com.apirest.entregable2.servicio;

import com.apirest.entregable2.modelo.Producto;
import com.apirest.entregable2.modelo.Usuario;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioProducto {

    //CRUD
    public Producto crearProducto(Producto producto);

    public List<Producto> obtenerProductos();

    public Optional<Producto> obtenerProductoPorId(String id);
    public Producto actualizarProducto(Producto producto);
    public Producto actualizarParteProducto(Producto producto,Producto productoActualizar);
    public  void borrarProductoPorID (String id);
    public List<Usuario> obtenerUsuariosProducto(String idProducto);
}
