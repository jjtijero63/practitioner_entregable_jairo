package com.apirest.entregable2.servicio;

import com.apirest.entregable2.modelo.Producto;
import com.apirest.entregable2.modelo.Usuario;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface ServicioUsuario {

    public Usuario creaUsuarioDeProducto (Producto p , Usuario usuario);
    public Usuario actualizaUsuarioDeProducto (Producto p , Usuario usuario);
    public Usuario actualizaParteUsuarioDeProducto (Producto p ,/*String idUsuario,*/ Usuario usuario);
    public boolean borrarUsuarioDeProducto (Producto p ,long idUsuario/*, Usuario usuario*/);
}
